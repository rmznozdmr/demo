package com.sky.service;


import com.sky.model.Channel;

import java.util.List;

public interface ChannelService {

    Channel findById(Long id);

    Channel findByName(String name);

    void saveChannel(Channel Channel);

    void updateChannel(Channel Channel);

    void deleteChannelById(Long id);

    void deleteAllChannels();

    List<Channel> findAllChannels();

    boolean isChannelExist(Channel Channel);
}