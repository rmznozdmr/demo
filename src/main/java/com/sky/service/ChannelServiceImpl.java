package com.sky.service;


import com.sky.model.Channel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sky.repository.ChannelRepository;
import javax.transaction.Transactional;
import java.util.List;

@Service("channelService")
@Transactional
public class ChannelServiceImpl implements ChannelService{

    @Autowired
    private ChannelRepository ChannelRepository;

    public Channel findById(Long id) {
        return ChannelRepository.findOne(id);
    }

    public Channel findByName(String name) {
        return ChannelRepository.findByName(name);
    }

    public void saveChannel(Channel Channel) {
        ChannelRepository.save(Channel);
    }

    public void updateChannel(Channel Channel){
        saveChannel(Channel);
    }

    public void deleteChannelById(Long id){
        ChannelRepository.delete(id);
    }

    public void deleteAllChannels(){
        ChannelRepository.deleteAll();
    }

    public List<Channel> findAllChannels(){
        return ChannelRepository.findAll();
    }

    public boolean isChannelExist(Channel Channel) {
        return findByName(Channel.getName()) != null;
    }

}
