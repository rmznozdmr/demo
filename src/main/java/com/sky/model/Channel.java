package com.sky.model;


import javax.persistence.*;
import javax.xml.bind.annotation.*;

@Entity
@Table(name = "channel")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Channel {

    @Id
    @GeneratedValue()
    private long id;

    @Column(nullable = false)
    private String name;

    @Column()
    private String description;

    @Column()
    String url;

    @Column()
    String image;

    @Column()
    private int rating;

    public Channel() {
    }

    public Channel(String name, String description,String url, int rating, String image) {
        this.name = name;
        this.description = description;
        this.rating = rating;
        this.url = url;
        this.image = image;
    }

    public long getId() {
        return this.id;
    }


    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Channel {" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", city='" + url + '\'' +
                ", rating=" + rating +
                '}';
    }
}
